<html>
    <head>
  	    <meta name="lera" content="text/html: charset=utf-8">
  	    <title>Back4</title>
        <meta name="lera" content="text/html: charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  	 
        <style>
            #error {
                 border: 2px solid red;
                 color: red;
            }

            #content {
                 width: 500px;
                 padding: 15px;
                 background-color: rgb(158, 216, 218);
                 font-weight: 500;
            }
            #mychek {
                 margin: 25px;
            }
            h1 a {
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="container" id="content">
			<form method="post" action="index.php" >
                <?php
                if(!empty($messages['login_and_password'])){
                    print($messages['login_and_password']);
                }
                if (!empty($messages['save'])) {
                    print($messages['save']);
                }
                ?>
				<div class="form-group">
                    <?php
                    $ERROR='';
                    $name='';
                    if (!empty($messages['name'])) {
                        print($messages['name']);
                        $ERROR='error';
                    }
                    if(!empty($values['name'])){
                        $name=$values['name'];
                    }
                    ?>
                    Имя
                    <input name="name" class="form-control, <?php print $ERROR?>" value="<?php print $name?>">
				</div>
                </br>
			<div class="form-group">
                    <?php
                    $ERROR='';
                    $mail='';
                    if (!empty($messages['mail'])) {
                        print($messages['mail']);
                        $ERROR='error';
                    }
                    if(!empty($values['mail'])){
                        $mail=$values['mail'];
                    }
                    ?>
					Email:
                    <input name="mail" value="<?php print $mail?>" class="form-control, <?php print $ERROR?>">
				</div>
                </br>
				<div class="form-group">
                    <?php
                    $ERROR='';
                    if (!empty($messages['year'])) {
                        print($messages['year']);
                        $ERROR='error';
                    } ?>
                    Year of Birth:
                    <span class="<?php print $ERROR?>">
                        <select class="form-control" name="year" <?php if ($errors['year']) {print 'id="error"';} ?> >
                	<?php
                	$select=array(1998=>'', 1999 => '',2000 => '',2001 => '',2002 => '',2003 => '');
                	for($s=1998; $s<=2003; $s++){
                        if($values['year']==$s){
                                    $select[$s]='selected';
                                }
                            }
                     ?>
    				<?php for($i = 1998; $i < 2003; $i++) { ?>
    				<option value="<?php print $i; ?>" <?php print $select[$i]?> ><?= $i; ?></option>
    				<?php } ?>
  				</select>

                    </span>
				</div>
                </br>
				<div class="form-group">
                    <?php
                    $ERROR='';
                    $sex='';
                    if (!empty($messages['sex'])) {
                        print($messages['sex']);
                        $ERROR='error';
                    }
                    if(!empty($values['sex'])){
                        $sex=$values['sex'];
                    }
                    ?>
                Sex:    <span class="<?php print $ERROR?>">
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="M" name="sex"<?php if($sex=='M') {print'checked';} ?> >Male
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="F" name="sex"<?php if($sex=='F') {print'checked';} ?> >Female
                        </label>
                    </div>
                    </span>
                </div>
                </br>
                <div  class="form-group">
                    <?php
                    $ERROR='';
                    if (!empty($messages['limbs'])) {
                        print($messages['limbs']);
                        $ERROR='error';
                    }
                    ?>
                    Limbs:<?php
                    $select_limbs=array(1=>'',2=>'',2=>'',3=>'',4=>'');
                    if(!empty($values['limbs'])){
                        for($s=1;$s<=4;$s++){
                            if($values['limbs']==$s){
                                $select_limbs[$s]='checked';break;
                            }
                        }
                    }
                    ?>
                    <span class="<?php print $ERROR?>">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" value="1" name="limbs" <?php print $select_limbs[1]?>>1
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" value="2" class="form-check-input" name="limbs" <?php print $select_limbs[2]?>>2
                                </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" value="3" name="limbs" class="form-check-input" <?php print $select_limbs[3]?>>3
                                </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" value="4" name="limbs" class="form-check-input" <?php print $select_limbs[4]?>>4
                                </label>
                        </div>
                    </span>
                </div>
                </br>
                </br>
                    <div class="form-group">
                        <?php
                        $ERROR='';
                        $BIO='';
                        if (!empty($messages['biography'])) {
                            print($messages['biography']);
                            $ERROR='error';
                        }
                        if(!empty($values['biography'])){
                            $BIO=$values['biography'];
                        }
                        ?>
                        <p class="<?php print $ERROR?>" >
                        <label for="comment">biography:</label>
                            <textarea class="form-control" rows="5" name="biography" <?php if ($errors['biography']) {print 'id="error"';} ?>><?php print $values['biography']; ?></textarea>

                        </p>
                    </div>
                </br>
                    <div id="check"  >
                    <?php
                    $ERROR='';
                    $check='';
                    if (!empty($messages['check'])) {
                        print($messages['check']);
                        $ERROR='error';
                    }
                    if(!empty($values['check'])){
                        $check='checked';
                    }
                    ?>
                    <span class="<?php print $ERROR?>" >С контрактом ознакомлен
					    <input type="checkbox" name="check"  value="yes" <?php print $check?>>
                    </span>
                </div>
                </br>
                <input type="submit" value="Отправить">
			</form>
            <?php   
                    if(!empty($_SESSION['login'])){
                        print('<form method="POST" action="login.php"><input type="submit" name="exit" value="Выход"></form>');
                    }
                ?>

		</div>	

  </body>
</html>
