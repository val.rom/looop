<?php
	$user = 'u20377';
	$pass = '5006255';
	$db = new PDO('mysql:host=localhost;dbname=u20377', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
	try {
		$stmt = $db->prepare("SELECT * FROM admin");
		$stmt->execute();
	}
	catch(PDOException $e) {
        print ('Ошибка : ' . $e->getMessage());
        exit();
    }

	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$login = $rows[0]['login'];
	$passw = $rows[0]['password'];


	if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) || $_SERVER['PHP_AUTH_USER'] != $login || md5($_SERVER['PHP_AUTH_PW']) != $passw) {
		header('HTTP/1.1 401 Unanthorized');
		header('WWW-Authenticate: Basic realm="My site"');
		print ('<h1>401 Требуется авторизация.</h1>');
		exit();
	}

	if ($_SERVER['REQUEST_METHOD'] == 'GET') {
		$stmt = $db->prepare("SELECT id, name, login, password, mail, birth, sex, limbs, bio FROM application");
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$values = [];
		foreach ($users as $user) {
			$ID = !empty($user['id']) ? $user['id'] : '';
			$name = !empty($user['name']  || preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $user['name'])) ? strip_tags($user['name']) : '';
			$email = !empty($user['mail']) ? strip_tags($user['mail']) : '';
			$date = (!empty($user['birth']) || preg_match('/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/u', $user['birth'])) ? $user['birth'] : '';
			$sex = !empty($user['sex']) ? $user['sex'] : '';
			$con = !empty($user['limbs']) ? $user['limbs'] : '';
			$bio = !empty($user['bio']) ? strip_tags($user['bio']) : '';
			$values[$user['id']] = [
				$ID, 
				$name, 
				$email, 
				$date, 
				$sex, 
				$con, 
				$bio, 
			];
		}
		$kolvo = count($values);
		include ('adm.php');
	} 
	else {
		$idRem = [$_POST['idRem']];
		$user = 'u20377';
		$pass = '5006255';
		$db = new PDO('mysql:host=localhost;dbname=u20377', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		try {
			$stmt = $db->prepare("SELECT * FROM application WHERE id = ?;");
			$stmt->execute($idRem);
		}
		catch(PDOException $e) {
			print ('Ошибка : ' . $e->getMessage());
			exit();
		}
		
		$userRemove = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (!empty($userRemove)) {
			$stmt = $db->prepare('DELETE FROM application WHERE id = ?');
			$stmt->execute($idRem);
			setcookie('save', '1');
			header('Location: admin.php');
		} 
		else {
			setcookie('idError', 1, 0);
			header('Location: admin.php');
		}
	}
?>
